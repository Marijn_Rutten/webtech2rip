//JSON : js object notation
var App = function () {
    "use strict";
    var stations, myApp, url;
    this.showStations = function () {


        //kijken of er iets is opgeslagen omdat het op de server zou kunnen staan en het is sneller
        if (localStorage.getItem("stations") !== null) {
            var stations = JSON.parse(localStorage.getItem("stations"));
        } else {
            //get it from server!
        }
        url = "http://api.irail.be/stations/?lang=NL&format=json";
        $.ajax( {
          type: 'GET',
          url: url,
          success: function ( resp ) {
              stations = resp.station;

              localStorage.setItem("stations", JSON.stringify(stations));

              $.each(stations, function (key, station){
                  console.log(station.name);
              });


          },
          error: function() {

          }
        });
    };


};

myApp = new App();
myApp.showStations();
