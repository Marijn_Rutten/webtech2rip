# IMD webtech 2 repository

Webtech 2 Labs
=============

###Oplossingen van Marijn Rutten voor het vak webtechnologie 2:
####Inhoud:
1. Lab : GIT to wrok!.
2. Lab : Mastering CSS3 animations and transitions.
3. Lab : Advanced Javascript part 1.
4. Lab : API's building an app prototype.
5. Lab : Node.js.
6. Lab : Angular.js.
7. Lab : SASS + BEM.
8. Lab : Gulp.js.

### Labo 1: GIT to Work! ###

#### Inhoud opdracht

<dl>
  <dd>
  	We moesten een team vormen van ongeveer 4 mensen en moesten een website maken waarin we kookrecepten moesten laten zien. Dit moesten we doen met HTML en CSS.
	De hoofdzaak was leren werken met GIT en samen met anderen een repository delen aanpassen en daarna weer de aanpassingen delen met de teamleden. In de toekomst zouden we ook met GIT moeten werken tijdens de lessen en zelfs in de professionele wereld. Op het einde moesten we dan een pull request doen naar de repository van de docent.
  </dd>
</dl>
  
#### Wat heb ik geleerd?

<dl>
  <dd>
	Tijdens deze les heb ik leren werken met GIT. Hoewel ik al eens met git gewerkt had was het goe dom dit nog eens op te frissen. GIT is een manier om code met mensen te delen en ook een soort van backup. Op deze manier kan je samenwerken met andere developers in je groep, wat wel handig is. In het begin was ging het wat stroef met de opdracht maar uiteindelijk was het wel gelukt.
  </dd>
</dl>

[Link](https://github.com/MarijnR/ayecaptain) naar repository die we moesten aanmaken.
  
#### Notities

<dl>
  <dd>
	git clone url = repository afhalen en in map plaatsen
	git pull = aanpassingen ophalen
	git add bestand = bestanden toevoegen aan de huidige map
	git commit -m "wat aangepast" = een beschrijving toevoegen aan de aanpassingen die je hebt gemaakt. altijd goede commentaar schrijven zo kan je teamgenoot het beter begrijpen.
	git status = kijkt wat het verschil is tussne de online en locale bestanden.
	git push = aanpassingen op je online repository toepassen.

	Branching Blangrijk!!
	git branch = laat alle branchen zien.
	git checkout -b naam = maakt branch aan en gaat ernaartoe.
	git checkout naam = van branch wisselen.
	git merge naam = samenvoegen van de branchen.
  </dd>
</dl>

* * *

### Lab 2: Mastering CSS3 animations and transitions ###

#### Inhoud opdracht

<dl>
  <dd>
  	Bij deze opdracht kregen we een paar voorbeelden die we zo goe mogelijk moesten namaken met CSS3 animations en transitions. Het was de bedoeling dat we animaties onder de knie zouden krijgen. Er waren 4 voorbeelden die we moesten nabouwen.
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>We hebben geleerd dat we met CSS3 veel kunnen doen en dat we met animaties de website veel aangenamer maken. Dit met nadruk op UX. We hebben geleerd wanneer we transitions moeten gebruiken en wanneer we Animations moeten gebruiken. Transitions kunnen niet automatisch afspelen en hebben een trigger nodig via JS. Animations kunnen automatisch orden afgespeeld. We hebben ook gezien dat transform beter is voor performence en zeker ook voor mobiel.
  </dd>
</dl>

#### Notities
<dl>
	<dd>
	CSS animatie;
	Pattern.dk/sun/ -> iphone 
	Animations, transitions, transformation, … 
	</dd>
	<dd>
	Transition:
	Altijd van iets naar iets
	Kan niet automatisch afgespeeld worden dus moet getriggerd worden door iets.
	</dd>
	<dd>
	Cubic-bezier(waarde.waarde,waarde.waarde) -> website
	Door margin-left te gebruiken om iets te verplaatsen gaat de prerformence van de app naar beneden.
	Debetere manier is transformations!
	</dd>
	<dd>
	Transformations:
	Beter voor performance zeker voor mobiel.
	Kan ook 3D
	</dd>
	<dd>
	Werken met transform function
	Transform: translateX(100px) scale(2);   !!!
	Translate = om te verplaatsne 
	Transform = kan scale zijn
	</dd>
	<dd>
	Animations
	Kan je loopen en kan ook automatisch starten
	Bijvoorbeeld parallax is met animations
	</dd>
</dl>
	-webkit-animation: move(naam) 10s infinite;
	@-webkit-keyframes move
	{
		0% {border-radius: 0 }
		100%{border-radius: 50% }
	}

	3D transforms

	-webkit-animation: flyIn 1s;
	Transform-origin: left;

	@-webkit-keyframes flyIn{
	0%
	{
		Transform: perspective(300px);
		rotateY(90deg);
	}
	100%
	{
	}
}

* * *

### Labo 3: Advanced JS part 1 ###

#### Inhoud opdracht

<dl>
  <dd>
  	Bij deze les hebben niet veel extra geleerd over het coderen zelf maar wel over een manier van programmeren. We moesten zelf een framework bouwen zoals jQuery. Dit zouden we later nodig hebben om code beter te kunnen begrijpen. We moesten een functie maken die een element kan selecteren zoals met get element by id maar zit moest ook werken met class. Ook moesten we een soort van klik event kunnen nadoen zoals je dat bij jQuery ook kan.
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	Ik heb geleerd dat je niet altijd jQuery moet gebruiken omdat jQuery een library is van functies die we soms niet allemaal nodig hebben. Daarom kan het handig zijn om zelf zo een framework te bouwen. Javascript is dynamisch opgebouwd dus als je bijvoorbeeld var mijnleeftijd = 23 doet dan gaat hij automatisch weten dat het een INT is.
  </dd>
</dl>

#### Notities

<dl>
	<dd>
	ECMA groep van javascript is en beetje de W3C van javascript
	Javascript is dynamicly typed
	Zet je var myvar = 3 dan weet javascript dat dit een nummer is dit is niet zo bij elke programmeer taal.
	Altijd met === vergelijken in plaats van met == om zeker te zijn.
	</dd>
</dl>

* * *

### Labo 4: API's building an app prototype ###

#### Inhoud opdracht

<dl>
  <dd>
  	Bij deze opdracht was het de bedoeling om een weerapp te bouwen. We moesten door gebruik te maken van de API developer.forecast.io. Hiermee moesten we werken met joon, geolocation, ajax, local storage en natuurlijk protoyping.
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	Deze opdracht vond ik heel interessant en leuk om te doen. Hoewel ik het moeilijk had om al die dingen zoals local storage, ajax enzo te gebruiken, het was een beetje veel wat op ons af kwam. Voor deze opdracht ben ik begonnen met het uitbouwen van de weerapp voor imkers die we in het vorige semester hebben moeten ontwerpen. Eens ik het door had was ik er snel mee weg. Ik was dan ook zeer blij dar ik alles heb werkende gekregen.
  </dd>
</dl>

#### Notities
<dl>
	<dd>
		Bij javascript hebben we geen class maar wel functies. Werken met `new Person();` om nieuwe instaniet. Werken met `Person.prototype`om functies bij in te stoppen.
		Ajax calls met jQuery en json niet XML.
	</dd>
</dl>

* * *

### Labo 5: Node.js ###

#### Inhoud opdracht

<dl>
  <dd>
  	Tijdens deze les is Dristrict01 een gastles komen geven over Node.js. We moesten een app bouwen dat zogezegd een frigo was. in de frigo moesten we items zoals drinken en grote enzo insteken. we moesten daarna aan de hand van een plus en min knop items kunnen verwijderen en toevoegen.Omdat alles na de gastles niet duidelijk was heeft de docent nog een extra les gegeven over node.js. Bij deze kregen we de opdracht een QandA app te bouwen. Je moest een naam kunnen invullen en een vraag en deze moest dan tevoorschijn komen in de vragen sectie. Hierop vinden dan andere mensen reageren.e
  </dd>
 </dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	We hebben express, mongo, node en socket.io geleerd. Deze dingen moesten we allemaal installeren met `npm install`. Deze les was voor mij heel moeilijk, ik was mee tijdens de les maar de opdracht achteraf maken was moeilijk. Het is enorm veel wat je ineens moet kennen, naar mijn mening zou het beter zijn geweest om alles stap voor stap te leren.
  </dd>
</dl>

#### Notities
<dl>
	<dd>
	mongoose
	npm = node package manager
	node-restfull
	express = framework
	</dd>
	<dd>
	npm install —-save
	</dd>
	<dd>
	alles wat in package.json zit kan je gebruiken in je applicatie om iets mee te zoen.
	</dd>
	<dd>
	io.sockets.emit(stuurt naar alle connected clients)
	//socket.emit(stuurt naar

	<dd>connectie maken met socket</dd>
	var socket = io.connect(‘http://localhost:3000’);
	</dd>
</dl>

* * *

### Labo 6: Angular.js ###

#### Inhoud opdracht

<dl>
  <dd>
  	Bij deze les is District01 een gastles komen geven over Angular.js. Op het einde kregen we ene opdracht om net zoals in node een soort frigo te maken waar je op plus en min kan drukken om iets toe te voegen en om iets te verwijderen enzo…
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	Angular dient om live dingen te laten gebeuren zoals bij een one page website dat je ergens op kan klikken zonder dat de pagina hoeft te herladen. Angular is client side. Bij deze oefening moesten we ook met een API werken die district01 zelf had gemaakt. Heel leuke les!
  </dd>
</dl>

#### Notities
<dl>
	<dd>
	Angular = cool
	is client side en allies word due op u browser sedan
	SPA’s singe page applications
	</dd>
	<dd>
	angular maakt gebruik van MVC - MVVM - MVW
	dus		 models(M		views(V		controllers(C
	</dd>
	<dd>
	angular werkt zonder jQuery
	geen javascript kennis nodig.
	</dd>
	<dd>
	template(html) en model worden samen een view
	</dd>
	<dd>
	in div ng = angular=‘imdApp'
	in js bestand var app = angular.module(‘imdApp’,[]);
	</dd>
	
	<dd>controller</dd>
	app.controller(myController’, function(){
		
	 });

	<dd>
	$scope = is de verbinding tussen controller en view
	$watch  kijkt naar de veranderingen op de scope
	newvalue, oldvalue om de waarder binnen te pakken
	</dd>
</dl>

* * *

### Labo 7: SASS + BEM ###

#### Inhoud Les

<dl>
  <dd>
  	Bij deze les hebben we leren werken met een nieuwe manier van layouten van je website. SASS dient eigenlijk om CSS te bouwen makkelijker te maken.
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	SASS is een CSS-preprocessor er bestaan er verschillende zoals LESS en stylus. Wij hebben leren werken met SASS.
  </dd>
  <dd>
  	BEM is een manier van programmeren om het voor mededevelopers makkelijker te maken om je code te leren.
  </dd>
</dl>

#### Notities
<dl>
	<dd>
	#text is niet duidelijk omdat als iemand anders moet komen helpen die snapt da niet.
</dd>
<dd>
	#articlemain probeer zo veel mogelijk met classes te werken
</dd>
	<dd>
	BEM, SMACSS, OOCSS
	Zorg dat je een structuur kunt gebruiken.
	</dd>
</dl>

* * *

### Labo 8: Gulp.js ###

#### Inhoud Les

<dl>
  <dd>
  	Hier hebben we leren werken met gulp.js
  </dd>
</dl>

#### Wat heb ik geleerd?

<dl>
  <dd>
  	We hebben geleerd dat gulp.js taken automatisch kan uitvoeren. Deze taken kunnen zijn css compileren, images automatisch aanpassen voor verschillende schermgrote, javascript compileren en nog veel meer.
	</dd>
</dl>

#### Notities
<dl>
	<dd>
		Installeren enkel lokaal
	</dd>
	npm install -g gulp --save-dev
	<dd>
		Om gulp te starten
	</dd>
	gulp
	<dd>
		configuratie file
	</dd>
	var gulp = require('gulp');
	var minifyCss = require('gulp-minify-css');

	gulp.task('default',['css']);

	gulp.task('css', function() {
	  // place code for your default task here
	  gulp.src('styles/*.css')
	    .pipe(minifyCss({compatibility: 'ie8'}))
	    .pipe(gulp.dest('dist'));
	  console.log("css");

	  gulp.task('watch', function(){
	  	gulp.watch('./css/*.css', ['css']);
	  });
	});
</dl>