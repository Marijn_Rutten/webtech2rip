$(document).ready(function(){
	$(".drink").on("click", function(e){
		// 1 - fanta of cola
		var drink = $(this).data("drink");
		console.log(drink);
		
		// 2 - sockets versturen naar server
		var vote = { "drink": drink };
		var socket = io.connect('http://localhost:3000');
		socket.emit('vote', vote);

	});
});